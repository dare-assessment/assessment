module.exports = {
	transform: {
		'^.+\\.jsx?$': 'babel-jest'
	},
	testPathIgnorePatterns: [ '<rootDir>/build/', '<rootDir>/node_modules/' ],
	setupFiles: [ 'dotenv/config' ],
	testEnvironment: 'node'
};
