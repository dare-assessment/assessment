# AXA DARE Node.js code assessment

## Objective

The goal of this assessment is to build an API REST following the swagger specs found at:
<a href="https://dare-nodejs-assessment.herokuapp.com/assessment-swagger/static/index.html" target="_blank">API DARE</a>


That means you need to deliver a API REST with the same API REST contract than the previous swagger details.

## Dependencies

- Babel for transpiling to ES6
- Axios for HTTP async request
- Dotenv for environment values
- Winston for logging
- Express for Server
- SuperTest for testing API endpoints
- Jest for testing components
- Swagger for OpenAPI markup
- AirBnb for liting
- Node-Cache for caching requests

---

## Table of Contents (Optional)

- [Installation](#installation)
- [Features](#features)
- [FAQ](#faq)

---

## Installation

### Clone

- Clone this repo: `git clone https://gitlab.com/dare-assessment/assessment.git`

### Setup

> Change .env.example to .env

Linux

```shell
$ mv .env.example .env
```

Windows

```shell
$ rename .env.example .env
```

> Update and install this package first

```shell
$ cd assessment
$ yarn install
```

> Then we can start the production server (There is also PM2 installed if required)

```shell
$ yarn prod
```

> After that, we need to go to

http://localhost:3050/api/v1/docs

---

## Features

- I've use a basic MVC for this, because of the size (I love DI by the way)
- Request are saved in logs will be saved in ./logs/ (Can be changed in .env)
- Selected 'dotenv' vs 'config' package by lack of variables in this project
- I've use Yarn Workspaces, this means the folder common is copied to node_modules to use referencial location

## Tests

- To execute test suit

```shell
$ yarn test
```
---

## Support

Any concerns please contact meroni.damian@gmail.com

---