/* Dotenv */
import dotenv from 'dotenv';
dotenv.config();

/* Winston */
import winston from 'winston';
import expressWinston from 'express-winston';

/* Express */
import express from 'express';
import cors from 'cors';
import createError from 'http-errors';
import NotFoundError from '@businessinsight/common/exception/NotFoundError';

/* Swagger*/
import { serve, setup } from 'swagger-ui-express';
import YAML from 'yamljs';

/* Middlewares */
import authMiddleware from '@businessinsight/common/middleware/auth';

/* Routes */
import Auth from './routes/auth';
import Client from './routes/client';
import Policy from './routes/policy';

/* Express */
const app = express();

/* Swagger */
const swaggerDocument = YAML.load('./swagger.yaml');
swaggerDocument.host = process.env.APP_HOST_IP
	? `${process.env.APP_HOST_IP}:${process.env.APP_HOST_PORT}`
	: 'localhost:3005';

swaggerDocument.schemes = [ 'http' ];
/* End Swagger */

app.use(cors());

app.use(express.json());
app.use(
	express.urlencoded({
		extended: false
	})
);

app.use(
	expressWinston.logger({
		transports: [
			new winston.transports.Console(),
			new winston.transports.File({ filename: process.env.LOG_DIR + 'logging.log' })
		],
		format: winston.format.combine(winston.format.colorize(), winston.format.json()),
		meta: true,
		msg: 'HTTP {{req.method}} {{req.url}}',
		expressFormat: true,
		colorize: false,
		ignoreRoute: function(req, res) {
			return false;
		}
	})
);

app.use(process.env.APP_LOCAL_API_ROUTE + '/login', Auth);
app.use(process.env.APP_LOCAL_API_ROUTE + '/clients', authMiddleware, Client);
app.use(process.env.APP_LOCAL_API_ROUTE + '/policies', authMiddleware, Policy);
app.use(process.env.APP_LOCAL_API_ROUTE + '/docs', serve, setup(swaggerDocument));

app.use(function(req, res, next) {
	next(createError(404));
});

app.use(function(err, req, res, next) {
	if (err instanceof NotFoundError) {
		return res.status(404).json({ error: err.message });
	}

	if (req.app.get('env') === 'development') {
		return res.status(err.status || 500).json({ error: err.message });
	}
	return res.sendStatus(err.status || 500);
});

process.on('unhandledRejection', (error) => {
	console.error('unhandledRejection', error.message);
});

export default app;
