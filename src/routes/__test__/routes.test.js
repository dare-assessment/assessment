import request from 'supertest';
import app from '../../app';
import getLoginToken from '@businessinsight/common/helper/getLoginToken';

describe('Get Endpoints', () => {
	it('should return clients list', async () => {
		const token = await getLoginToken();

		const res = await request(app).get('/api/v1/clients').auth(token, { type: 'bearer' });

		expect(res.statusCode).toEqual(200);
	});
	it('should return policies list', async () => {
		const token = await getLoginToken();

		const res = await request(app).get('/api/v1/policies').auth(token, { type: 'bearer' });

		expect(res.statusCode).toEqual(200);
	});
	it('should fail for no token on policies list', async () => {
		const res = await request(app).get('/api/v1/policies');

		expect(res.statusCode).toEqual(401);
	});
});
