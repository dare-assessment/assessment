import { Router } from 'express';
import { asyncMiddleware } from '@businessinsight/common/middleware/async';

import { login } from '../services/auth/login.js';

const router = Router();

router.post(
	'',
	asyncMiddleware(async (req, res) => {
		const { client_id, client_secret } = req.body;

		if (!client_id) {
			return res.status(401).json({ statusCode: 401, message: 'client_id is required' });
		}

		if (!client_secret) {
			return res.status(401).json({ statusCode: 401, message: 'client_secret is required' });
		}

		// The OAuth 2.0 Authorization: https://tools.ietf.org/html/rfc6750#section-2.2
		if (req.get('content-type') !== 'application/x-www-form-urlencoded') {
			return res.status(401).json({ statusCode: 401, message: 'Invalid header type' });
		}

		const data = await login(client_id, client_secret);

		res.json(data);
	})
);

export default router;
