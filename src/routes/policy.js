import { Router } from 'express';
import { asyncMiddleware } from '@businessinsight/common/middleware/async';

import { getPolicyList, findPolicy } from '../services/policy/policy.js';

const router = Router();

router.get(
	'/',
	asyncMiddleware(async (req, res) => {
		const { limit } = req.query;

		let params = {
			token: req.token
		};

		if (limit) {
			if (isNaN(limit)) return res.status(400).json({ statusCode: 400, error: 'Limit should be numeric' });
			else params.limit = limit;
		}

		const data = await getPolicyList(params);

		res.json(data);
	})
);

router.get(
	'/:id',
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		let params = {
			token: req.token
		};

		if (!id) {
			return res.status(400).json({ statusCode: 400, error: 'Missing id' });
		} else {
			params.id = id;
		}

		const data = await findPolicy(params);

		res.json(data);
	})
);

export default router;
