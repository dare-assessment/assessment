import { Router } from 'express';
import { asyncMiddleware } from '@businessinsight/common/middleware/async';

import { getClientList, findClient, findClientPolicies } from '../services/client/client.js';

const router = Router();

router.get(
	'/',
	asyncMiddleware(async (req, res) => {
		const { limit, name } = req.query;

		let params = {
			token: req.token
		};

		if (limit) {
			if (isNaN(limit)) return res.status(400).json({ statusCode: 400, error: 'Limit should be numeric' });
			else params.limit = limit;
		}

		if (name) {
			params.name = name;
		}

		const data = await getClientList(params);

		res.json(data);
	})
);

router.get(
	'/:id',
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		let params = {
			token: req.token
		};

		if (!id) {
			return res.status(400).json({ statusCode: 400, error: 'Missing id' });
		} else {
			params.id = id;
		}

		const data = await findClient(params);

		res.json(data);
	})
);

router.get(
	'/:id/policies',
	asyncMiddleware(async (req, res) => {
		const { id } = req.params;

		let params = {
			token: req.token
		};

		if (!id) {
			return res.status(400).json({ statusCode: 400, error: 'Missing id' });
		} else {
			params.id = id;
		}

		const data = await findClientPolicies(params);

		res.json(data);
	})
);

export default router;
