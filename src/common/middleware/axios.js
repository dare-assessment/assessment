const { setup } = require('axios-cache-adapter');

module.exports = function(token = null) {
	const axiosSetup = {
		baseURL: process.env.REMOTE_API,
		headers: {
			'content-type': 'application/json',
			'Access-Control-Allow-Origin': '*'
		},
		cache: {
			maxAge: 15 * 60 * 1000
		}
	};

	if (token) {
		axiosSetup.headers.common = { Authorization: `bearer ${token}` };
	}

	return setup(axiosSetup);
};
