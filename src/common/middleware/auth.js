const decodeJwt = require('../helper/decodeJwt');

module.exports = async (req, res, next) => {
	try {
		const { authorization } = req.headers;

		if (!authorization) throw new Error('You must send an Authorization header');

		const bearer = authorization.split(' ');
		const bearerToken = bearer[1];

		let tokenDecoded = decodeJwt(bearerToken);

		if (!tokenDecoded) throw new Error('Invalid token format.');

		let currentDate = new Date().getTime() / 1000;
		let tokenDate = new Date(tokenDecoded.exp).getTime();

		if (currentDate > tokenDate) throw new Error('Token expired. Request a new one.');

		req.token = bearerToken;

		next();
	} catch (error) {
		res.status(401).send({
			statusCode: 401,
			message: error.message
		});
	}
};
