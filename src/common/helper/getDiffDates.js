module.exports = (date1, date2) => {
	try {
		let t1 = new Date(date1);
		let t2 = new Date(date2);

		let dif = t1.getTime() - t2.getTime();

		return Math.round(dif / 1000);
	} catch (e) {
		return false;
	}
};
