const jwt_decode = require('jwt-decode');

module.exports = function(token) {
	try {
		if (!token) {
			return null;
		}

		return jwt_decode(token);
	} catch (e) {
		return null;
	}
};
