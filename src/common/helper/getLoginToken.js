const { login } = require('../../services/auth/login');

module.exports = async () => {
	const loginResponse = await login(process.env.TEST_USER, process.env.TEST_USER_PASS);

	expect(loginResponse.hasOwnProperty('token')).toEqual(true);

	if (loginResponse.token) {
		return loginResponse.token;
	}

	return false;
};
