module.exports = (e) => {
	switch (e.response.status) {
		case 400: {
			return {
				statusCode: 400,
				message: 'Bad request'
			};
		}
		case 401: {
			return {
				statusCode: 401,
				message: 'Unauthorized'
			};
		}
		case 404: {
			return {
				statusCode: 404,
				message: 'Not found'
			};
		}
		default: {
			return {
				statusCode: 500,
				message: 'External API error'
			};
		}
	}
};
