import { getPolicyList, getPolicies } from '../policy';
import getLoginToken from '@businessinsight/common/helper/getLoginToken';

describe('Get policy list', () => {
	it('Return policy list has elements', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getPolicies(token);

			expect(data.length > 0).toEqual(true);
		}
	});

	it('Query default has 10', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getPolicyList({ token: token });

			expect(data.length).toEqual(10);
		}
	});

	it('Query only 5', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getPolicyList({ token: token, limit: 5 });

			expect(data.length).toEqual(5);
		}
	});

	it('Query only 0, should recive 10 (By default)', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getPolicyList({ token: token, limit: 0 });

			expect(data.length).toEqual(10);
		}
	});
});
