import axios from '@businessinsight/common/middleware/axios';
import genericErrorCatch from '@businessinsight/common/helper/genericErrorCatch';
import getDiffDates from '@businessinsight/common/helper/getDiffDates';

import CacheService from '../cache/cache';

const Cache = new CacheService();

export const getPolicyList = async (args) => {
	const { token, limit } = args;

	if (!token) {
		return false;
	}

	try {
		const Policies = await getPolicies(token);

		if (Policies.statusCode) {
			return Policies;
		}

		if (Policies && Policies.length) {
			let theLimit = limit ? limit : process.env.DEFAULT_POLICIES;

			// If Policies amount is inferior to the limit, we set the new limit to the policices amount
			theLimit = Policies.length < theLimit ? Policies.length : theLimit;

			let chunk = Policies.slice(0, theLimit);

			return chunk.map((item) => {
				return {
					id: item.id,
					amountInsured: item.amountInsured,
					email: item.email,
					inceptionDate: item.inceptionDate,
					installmentPayment: item.installmentPayment
				};
			});
		} else {
			return {
				statusCode: 200,
				message: 'Empty data'
			};
		}
	} catch (e) {
		_handleApiError(e);
	}
};

export const findPolicy = async (args) => {
	const { token, id } = args;

	if (!token) {
		return false;
	}

	try {
		const Policies = await getPolicies(token);

		if (Policies.statusCode) {
			return Policies;
		}

		if (Policies && Policies.length) {
			let found = Policies.filter((item) => item.id === id);

			return found
				? found
				: {
						statusCode: 404,
						message: 'Not found'
					};
		} else {
			return {
				statusCode: 200,
				message: 'Empty data'
			};
		}
	} catch (e) {
		_handleApiError(e);
	}
};

export const getPolicies = async (token) => {
	if (!token) {
		return false;
	}

	let cached = Cache.get('policies');

	if (cached) {
		return cached;
	}

	const axiosInstance = axios(token);

	try {
		const res = await axiosInstance.get('/policies');

		let diffDates = getDiffDates(res.headers.expires, res.headers.date);
		let cacheTtl = diffDates ? diffDates : process.env.DEFAULT_CACHE_TIME;

		Cache.set('policies', res.data, cacheTtl);

		return res.data;
	} catch (e) {
		return _handleApiError(e);
	}
};

const _handleApiError = (e) => {
	if (e.response) {
		return genericErrorCatch(e);
	}
};
