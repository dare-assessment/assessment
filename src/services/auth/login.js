import axios from '@businessinsight/common/middleware/axios';
import decodeJwt from '@businessinsight/common/helper/decodeJwt';
import genericErrorCatch from '@businessinsight/common/helper/genericErrorCatch';

const axiosInstance = axios();

export const login = async (user, pass) => {
	try {
		const { data } = await axiosInstance.post('/login', {
			client_id: user,
			client_secret: pass
		});

		if (data.token) {
			let tokenDecoded = decodeJwt(data.token);

			if (!tokenDecoded) {
				return {
					code: 401,
					message: 'Invalid token format on remote API'
				};
			}

			if (tokenDecoded.exp) {
				let currentDate = new Date().getTime() / 1000;
				let tokenDate = new Date(tokenDecoded.exp).getTime();

				data.expires_in = Math.floor(tokenDate - currentDate);
			}

			return data;
		}

		return {
			code: 404,
			message: 'Token not found'
		};
	} catch (e) {
		if (e.response) {
			return genericErrorCatch(e);
		}
	}
};
