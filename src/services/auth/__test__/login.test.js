import { login } from '../login';

describe('Test login', () => {
	it('Expect login fail with empty credentials', async () => {
		const data = await login('', '');

		expect(Object.keys(data)).toHaveLength(2);
		expect(data.hasOwnProperty('statusCode')).toEqual(true);
		expect(data.statusCode).toEqual(401);
	});

	it('Expect login fail with bad credentials', async () => {
		const data = await login('fails', 'fail');

		expect(Object.keys(data)).toHaveLength(2);
		expect(data.hasOwnProperty('statusCode')).toEqual(true);
		expect(data.statusCode).toEqual(401);
	});

	it('Expect login success with good credentials', async () => {
		const data = await login(process.env.TEST_USER, process.env.TEST_USER_PASS);

		expect(Object.keys(data)).toHaveLength(3);
		expect(data.hasOwnProperty('token')).toEqual(true);
		expect(data.hasOwnProperty('type')).toEqual(true);
		expect(data.hasOwnProperty('expires_in')).toEqual(true);
	});
});
