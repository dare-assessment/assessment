import { getClients, getClientList, findClientPolicies } from '../client';
import getLoginToken from '@businessinsight/common/helper/getLoginToken';

describe('Get client by id', () => {
	it('Return client list has elements', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClients(token);

			expect(data.length > 0).toEqual(true);
		}
	});
});

describe('Client policies', () => {
	it('Return client policies', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await findClientPolicies({ token, id: 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb' });

			expect(data.length > 0).toEqual(true);
		}
	});
	it('Should return 404 not found', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await findClientPolicies({ token, id: 'test' });

			expect(Object.keys(data)).toHaveLength(2);
			expect(data.hasOwnProperty('statusCode')).toEqual(true);
			expect(data.statusCode).toEqual(404);
			expect(data.hasOwnProperty('message')).toEqual(true);
			expect(data.message).toEqual('Not found');
		}
	});
});

describe('Get client list', () => {
	it('Return client list has elements', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClients(token);

			expect(data.length > 0).toEqual(true);
		}
	});

	it('Query default has 10', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token });

			expect(data.length).toEqual(10);
		}
	});

	it('Query only 5', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token, limit: 5 });

			expect(data.length).toEqual(5);
		}
	});

	it('Query only 0, should recive 10 (By default)', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token, limit: 0 });

			expect(data.length).toEqual(10);
		}
	});
});

describe('Get client policies', () => {
	it('Return client list has elements', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClients(token);

			expect(data.length > 0).toEqual(true);
		}
	});

	it('Query default has 10', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token });

			expect(data.length).toEqual(10);
		}
	});

	it('Query only 5', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token, limit: 5 });

			expect(data.length).toEqual(5);
		}
	});

	it('Query only 0, should recive 10 (By default)', async () => {
		const token = await getLoginToken();

		if (token) {
			const data = await getClientList({ token: token, limit: 0 });

			expect(data.length).toEqual(10);
		}
	});
});
