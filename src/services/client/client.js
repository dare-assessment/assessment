import axios from '@businessinsight/common/middleware/axios';
import pick from 'lodash/pick';
import genericErrorCatch from '@businessinsight/common/helper/genericErrorCatch';
import getDiffDates from '@businessinsight/common/helper/getDiffDates';

import { getPolicies } from '../policy/policy.js';

import CacheService from '../cache/cache';

const Cache = new CacheService();

export const getClientList = async (args) => {
	const { token, limit, name } = args;

	if (!token) {
		return false;
	}

	try {
		const Clients = await getClients(token);

		if (Clients.statusCode) {
			return Clients;
		}

		const Policies = await getPolicies(token);

		if (Policies.statusCode) {
			return Policies;
		}

		if (Clients && Clients.length && Policies) {
			let theLimit = limit ? limit : process.env.DEFAULT_CLIENTS;

			// If Clients amount is inferior to the limit, we set the new limit to the clients amount
			theLimit = Clients.length < theLimit ? Clients.length : theLimit;

			// If name exists, we filter first only matching names, and then that we slice based in limit
			let chunk = (name ? Clients.filter((item) => item.name === name) : Clients).slice(0, theLimit);

			let uPolicies = [];

			chunk.map((item) => {
				uPolicies = _getPoliciesByUserid(item.id, Policies, [ 'id', 'amountInsured', 'inceptionDate' ]);

				if (uPolicies.length) {
					item.policies = uPolicies;
				}
			});

			return chunk;
		} else {
			return {
				statusCode: 200,
				message: 'Empty data'
			};
		}
	} catch (e) {
		_handleApiError(e);
	}
};

export const getClients = async (token) => {
	if (!token) {
		return false;
	}

	let cached = Cache.get('clients');

	if (cached) {
		return cached;
	}

	const axiosInstance = axios(token);

	try {
		const res = await axiosInstance.get('/clients');

		let diffDates = getDiffDates(res.headers.expires, res.headers.date);
		let cacheTtl = diffDates ? diffDates : process.env.DEFAULT_CACHE_TIME;

		Cache.set('clients', res.data, cacheTtl);

		return res.data;
	} catch (e) {
		return _handleApiError(e);
	}
};

export const findClient = async (args) => {
	const { token, id } = args;

	if (!token) {
		return false;
	}

	try {
		const Clients = await getClients(token);

		if (Clients.statusCode) {
			return Clients;
		}

		const Policies = await getPolicies(token);

		if (Policies.statusCode) {
			return Policies;
		}

		if (Clients && Clients.length) {
			let userFound = Clients.filter((item) => item.id === id)[0];

			if (!userFound) {
				return {
					statusCode: 404,
					message: 'Not found'
				};
			}
			if (Policies) {
				let uPolicies = _getPoliciesByUserid(userFound.id, Policies, [
					'id',
					'amountInsured',
					'inceptionDate'
				]);

				if (uPolicies.length) {
					userFound.policies = uPolicies;
				}
			}

			return userFound;
		} else {
			return {
				statusCode: 200,
				message: 'Empty data'
			};
		}
	} catch (e) {
		_handleApiError(e);
	}
};

export const findClientPolicies = async (args) => {
	const { token, id } = args;

	if (!token) {
		return false;
	}

	try {
		const Policies = await getPolicies(token);

		if (Policies.statusCode) {
			return Policies;
		}

		if (Policies && Policies.length) {
			let userPolicies = _getPoliciesByUserid(id, Policies, [
				'id',
				'amountInsured',
				'email',
				'inceptionDate',
				'installmentPayment'
			]);

			if (userPolicies.length === 0) {
				return {
					statusCode: 404,
					message: 'Not found'
				};
			}

			return userPolicies;
		} else {
			return {
				statusCode: 200,
				message: 'Empty data'
			};
		}
	} catch (e) {
		_handleApiError(e);
	}
};

const _getPoliciesByUserid = (userId, Policies, fields = null) => {
	let selected = [];

	Policies.reduce((result, policy) => {
		if (policy.clientId == userId) {
			selected.push(fields ? pick(policy, fields) : policy);
		}

		return result;
	}, []);

	return selected;
};

const _handleApiError = (e) => {
	if (e.response) {
		return genericErrorCatch(e);
	}
};
