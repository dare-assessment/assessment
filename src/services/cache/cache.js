import NodeCache from 'node-cache';

class Cache {
	constructor(ttlSeconds = 600) {
		this.cache = new NodeCache({
			stdTTL: ttlSeconds ? ttlSeconds : process.env.DEFAULT_CACHE_TIME,
			checkperiod: ttlSeconds ? ttlSeconds : process.env.DEFAULT_CACHE_TIME * 0.2,
			useClones: false
		});
	}

	get(key) {
		const value = this.cache.get(key);

		if (value) {
			return value;
		}

		return null;
	}

	set(key, obj, time) {
		return this.cache.set(key, obj, time);
	}

	del(keys) {
		this.cache.del(keys);
	}

	flush() {
		this.cache.flushAll();
	}
}

export default Cache;
