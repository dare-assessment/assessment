import app from '../app';

import { createServer } from 'http';

let port = normalizePort(process.env.APP_HOST_PORT || '4001');

app.set('port', port);

let server = createServer(app);

server.listen(port);

server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
	let iPort = parseInt(val, 10);

	if (isNaN(iPort)) {
		return val;
	}

	if (iPort >= 0) {
		return iPort;
	}

	return false;
}

function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	let bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

	switch (error.code) {
		case 'EACCES':
			console.error(`${bind} requires elevated privileges`);
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(`${bind} is already in use`);
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening() {
	var addr = server.address();
	var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

	console.log(`Listening on ${bind}`);
}
